<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$uploadsDir = 'resources/temp/';
if (isset($_POST['submit'])) {
    if (!empty($_FILES['file']['tmp_name'])) {
        $name = $_FILES['file']['name'];
        $file = $_FILES['file']['tmp_name'];
        $moved = move_uploaded_file($file, $uploadsDir.$name);

        if (!file_exists($uploadsDir.$name)) {
            die('Error locating file');
        }

        $fileToCopy = 'resources/gapedi.xlsx';
        $dest = 'resources/gapedi_'.date('Y_m_d').'_pricelist.xlsx';

        // if (!copy($fileToCopy, $dest)) {
        //     die("failed to copy $fileToCopy");
        // }

        $reader = IOFactory::createReaderForFile($uploadsDir.$name);
        $reader->setReadDataOnly(false);
        $reader->setLoadSheetsOnly(["Sheet1"]);
        $spreadSheet = $reader->load($uploadsDir.$name);
        $worksheet = $spreadSheet->getActiveSheet();
        $writableSpreadSheet = IOFactory::load($fileToCopy);
        $sheetToWriteTo = $writableSpreadSheet->getActiveSheet();

        $sheetToWriteTo->setCellValue('A4', date('d F Y'));
        
        $startingRow = 25;
        // \d+
        // [A-Z]+
        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE);
            foreach ($cellIterator as $cell) {
                try {
                    $style = $cell->getStyle();
                    $fillColor = $style->getFill()->getStartColor()->getRGB();
                    $fontColor = $style->getFont()->getColor()->getARGB();
                    $topBorder = $style->getBorders()->getTop()->getBorderStyle();
                    $bottomBorder = $style->getBorders()->getBottom()->getBorderStyle();
                    $leftBorder = $style->getBorders()->getLeft()->getBorderStyle();
                    $rightBorder = $style->getBorders()->getRight()->getBorderStyle();
                    $value = $cell->getValue();
                    $cellIndex = $cell->getCoordinate();
                    preg_match('/\d+/', $cellIndex, $digit);
                    preg_match('/[A-Z]+/', $cellIndex, $alpha);
                    $cellColumn = $alpha[0];
                    $cellRow = $digit[0];
                    $writeAt = $cellColumn.($cellRow+$startingRow);
                    if (($cellColumn === 'D' || $cellColumn === 'H') && is_numeric($value)) {
                        if ($fillColor !== 'FFFF00') {
                            $value = 'R' . number_format((int) $value + ((int) $value * 0.33), 2);
                        } else {
                            $value = 'R' . number_format((int) $value, 2);
                        }
                    }
                    $sheetToWriteTo->setCellValue($writeAt, $value);
                    $styleArray = [
                        'font'  => [
                            'bold'  => $style->getFont()->getBold(),
                            'color' => ['argb' => $fontColor]
                        ],
                        'borders' => [
                            'top' => [
                                'borderStyle' => $topBorder,
                            ],
                            'bottom' => [
                                'borderStyle' => $bottomBorder,
                            ],
                            'left' => [
                                'borderStyle' => $leftBorder,
                            ],
                            'right' => [
                                'borderStyle' => $rightBorder,
                            ],
                        ],
                    ];
                    if ($fillColor === 'FFFF00') {
                        $styleArray['fill'] = [
                            'fillType'  => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'startColor' => [
                                'rgb' => $style->getFill()->getStartColor()->getRGB(),
                            ]];
                    }
                    $sheetToWriteTo->getStyle($cellColumn.($cellRow+$startingRow))->applyFromArray($styleArray);
                } catch (Exception $e) {

                }
                
            }
            
        }
        
        $writer = new Xlsx($writableSpreadSheet);
        $writer->save($dest);

        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename='.basename($dest));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($dest));
        readfile($dest);

        unlink($dest);
        unlink('resources/temp/'.$name);

    }
}